var worker = {
	score: undefined,
	running: false,
	data: undefined,

	config: {
		tickRate: 50,
	},

	reset: function() {
		game.reset();
		config.draw = false;
		config.ai = true;
		worker.score = undefined;
	},

	respondToMainThread: function() {
		if (worker.score === undefined) {
			setTimeout(worker.respondToMainThread, worker.config.tickRate);
			return;
		}

		var response = {
			workerId: worker.data.workerId,
			score: worker.score,
			specimenId: worker.data.specimenId,
		};

		postMessage(response);
	},

	run: function() {
		// get started
		if (!worker.running) {
			worker.reset();
			tetron.specimen.applyWeights(worker.data.weights);
		}

		// not done
		if (worker.score === undefined) {
			var score = tetron.step();
			if (score === undefined) {
				worker.running = true;
				setTimeout(worker.run, worker.config.tickRate);
			}
			else {
				worker.score = score;
				worker.running = false;
			}
		// or done
		} else {
			worker.running = false;
			return worker.score;
		}
	},

	start: function(data) {
		worker.reset();
		worker.data = data;
		worker.score = undefined;
		if (data.tickRate !== undefined) worker.config.tickRate = data.tickRate;
		worker.run();
		worker.respondToMainThread();
	},
};

	onmessage = function(message) {
	self.importScripts('../tetris.js', '../tetron.js');
	// let's unzip it first
	var data = message.data;
	worker.start(data);
};