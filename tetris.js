var config = {
	draw: true,
	ai: false,
};

var colors = [
	[10,10,10],		// "black" (background)
	[255,0,0],		// red
	[0,255,0],		// green
	[0,0,255],		// blue
	[255,0,255],	// magenta
	[255,255,0],	// yellow
	[0,255,255],	// cyan
	[255,128,0],	// orange
	[255,255,255]	// white
];

var pieces = [
	[
		[0,0,0,0],
		[1,1,1,1],
		[0,0,0,0],
		[0,0,0,0],
	],
	[
		[2,2],
		[2,2],
	],
	[
		[3,0,0],
		[3,3,3],
		[0,0,0],
	], // L1
	[
		[0,0,4],
		[4,4,4],
		[0,0,0],
	], // L2
	[
		[5,5,0],
		[0,5,5],
		[0,0,0],
	], // S1
	[
		[0,6,6],
		[6,6,0],
		[0,0,0],
	], // S2
	[
		[0,7,0],
		[7,7,7],
		[0,0,0],
	], // T
];

var emptyRow = [colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]];

var grid = [
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]], // 10
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]],
	[colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0],colors[0]], // 20
];

var field = {
	height: 600,
	width: 600,
	bgcolor: 0,
	pixelSize: 30,
};

var currentPiece = {
	data: {
		position: {
			x: 0,
			y: 0,
		},

		type: undefined,
		shape: undefined,
	},

	getGridCoords: function() {
		var result = [];

		for (var rowNumber = 0; rowNumber < currentPiece.data.shape.length; rowNumber++) {
			var row = currentPiece.data.shape[rowNumber];
			for (var colNumber = 0; colNumber < row.length; colNumber++) {
				if (row[colNumber] !== 0) {
					var absX = currentPiece.data.position.x + colNumber;
					var absY = currentPiece.data.position.y + rowNumber;

					var myField = {x: absX, y: absY};
					result.push(myField);
				}
			}
		}

		return result;
	},

	/**
	 * object {x,y}
	 */
	occupiesField: function(field) {
		var currentFields = currentPiece.getGridCoords();

		for (var index = 0; index < currentFields.length; index++) {
			if (field.x === currentFields[index].x
				&& field.y === currentFields[index].y) return true;
		}

		return false;
	},

	checkCollision: function(side) {
		var xMod = 0;
		var yMod = 0;

		switch (side) {
			case "RIGHT":
				xMod = 1;
				break;
			case "LEFT":
				xMod = -1;
				break;
			case "ROTATE":
			// NYI
				return currentPiece.checkRotationCollision();
			case "BOTTOM":
			default:
				yMod = +1;
		}

		var currentFields = currentPiece.getGridCoords();

		for (var index = 0; index < currentFields.length; index++) {
			var field = currentFields[index];
			field.x += xMod;
			field.y += yMod;


			// pretty because debug.
			if (!isOnGrid(field)) {
				return true;
			}

			if (!isFieldEmpty(field) && !currentPiece.occupiesField(field)) {
				return true;
			}
		}

		return false;
	},

	checkRotationCollision: function() {
		var oldMatrix = deep_copy(currentPiece.data.shape);
		var newMatrix = rotateMatrix(currentPiece.data.shape);

		var result = false;
		currentPiece.removeFromGrid();
		currentPiece.data.shape = newMatrix;

		var coords = currentPiece.getGridCoords();

		for (var i = 0; i < coords.length; i++) {
			if (!isOnGrid(coords[i]) || !isFieldEmpty(coords[i])) {
				result = true;
				break;
			}
		}

		currentPiece.data.shape = oldMatrix;
		currentPiece.addToGrid();
		return result;
	},

	moveLeft: function() {
		if (!currentPiece.checkCollision("LEFT")) {
			currentPiece.removeFromGrid();
			currentPiece.data.position.x--;
			currentPiece.addToGrid();
			return true;
		}

		return false;
	},

	moveDown: function() {
		if (currentPiece.checkCollision("BOTTOM")) {
			currentPiece.finalize();
		} else {
			currentPiece.removeFromGrid();
			currentPiece.data.position.y++;
			currentPiece.addToGrid();
			return true;
		}

		return false;
	},

	moveRight: function() {
		if (!currentPiece.checkCollision("RIGHT")) {
			currentPiece.removeFromGrid();
			currentPiece.data.position.x++;
			currentPiece.addToGrid();

			return true;
		}

		return false;
	},

	rotate: function(times) {
		for (var i = 0; i < times; i++) {
			if (!currentPiece.checkCollision("ROTATE")) {
				currentPiece.removeFromGrid();

				currentPiece.data.shape = rotateMatrix(currentPiece.data.shape);
				currentPiece.addToGrid();
			} else return false;
		}

		return true;
	},

	drop: function() {
		while(currentPiece.moveDown()) {
		}
	},

	finalize: function() {
		game.spawnPiece();
		game.handleRowClears();
	},

	removeFromGrid: function() {
		var fields = currentPiece.getGridCoords();

		for (var currentFieldNum = 0; currentFieldNum < fields.length; currentFieldNum++) {
			var currentField = fields[currentFieldNum];

			if (!isFieldEmpty(currentField)) {
				setField(currentField,0);
			} else {
				return false;
			}
		}

		return true;
	},

	addToGrid: function() {
		var fields = currentPiece.getGridCoords();
		for (var currentFieldNum = 0; currentFieldNum < fields.length; currentFieldNum++) {
			var currentField = fields[currentFieldNum];

			if (isOnGrid(currentField)
				&& isFieldEmpty(currentField)) {
				setField(currentField, currentPiece.data.type+1);
			} else {
				return false;
			}
		}

		return true;
	},
};

var game = {
	score: 0,
	totalRowsCleared: 0,
	tickRate: 150,
	lastTick: 0,
	lost: false,
	nextPiecesBag: [],
	piecesDealt: 0,

	exportCurrentGameState: function() {
		var state = {
			lost: game.lost,
			score: this.score,
			totalRowsCleared: this.totalRowsCleared,
			piecesDealt: game.piecesDealt,
			rngSeed: RNG.seed,
			bag: this.nextPiecesBag.slice(), // performance
			grid: deep_copy(grid),
			data: deep_copy(currentPiece.data),
		};

		if (state.data === undefined) console.log('alert');
		return state;
	},

	importGameState: function(state) {
		if (state === undefined) console.trace('kaboom');
		this.lost = state.lost;
		this.score = state.score;
		this.totalRowsCleared = state.totalRowsCleared;
		this.piecesDealt = state.piecesDealt;
		RNG.seed = state.rngSeed;
		// previously exported game states changing because of stuff being passed by reference
		// has cost me way more time than it should have...
		this.nextPiecesBag = state.bag.slice();
		grid = deep_copy(state.grid);
		currentPiece.data = deep_copy(state.data);
	},

	reset: function() {
		currentPiece.data = {
			position: {
				x: 0,
				y: 0,
			},

			type: undefined,
			shape: undefined,
		};

		for (var y in grid) {
			grid[y] = deep_copy(emptyRow);
		}

		game.score = 0;
		game.totalRowsCleared = 0;
		config.ai = false;
		config.draw = true;
		game.start();
	},

	tick: function() {

		if (!game.lost) {
			if (time() >= game.lastTick + game.tickRate) {
				redrawField();
				game.lastTick = time();
			} else {
				setTimeout(this.tick, this.tickRate);
			}
		} //else console.log('cannot tick game: game is lost.');
	},

	start: function() {
		game.lastTick = time();
		game.lost = false;
		game.piecesDealt = 0;
		game.nextPiecesBag = [];
		RNG.reset();
		game.spawnPiece();
	},

	spawnPiece: function() {
		if (game.nextPiecesBag.length === 0) {
			game.nextPiecesBag = [0,1,2,3,4,5,6];
		}

		var random = Math.floor(RNG.generate() * game.nextPiecesBag.length);
		var nextPiece = game.nextPiecesBag.splice(random,1)[0];
		var size = pieces[nextPiece].length;

		currentPiece.data.type = nextPiece;
		currentPiece.data.shape = pieces[nextPiece];
		currentPiece.data.position.x = Math.floor((grid[0].length-size)/2);
		currentPiece.data.position.y = 0;

		var success = currentPiece.addToGrid();
		if (!success) game.lost = true;
		else game.piecesDealt++;
		return success;
	},

	awardClearScore: function(num) {
		game.totalRowsCleared += num;

		switch (num) {
			case 1:
				game.score += 40;
				break;
			case 2:
				game.score += 100;
				break;
			case 3:
				game.score += 300;
				break;
			case 4:
				game.score += 1200;
				break;
			default:
				break;
		}
	},

	handleRowClears: function() {
		currentPiece.removeFromGrid();
		var clears = 0;
		for (var row = grid.length - 1; row >= 0; row--) {
			while (isRowFull(row)) {
				for (var row2 = row; row2 >= 0; row2--) {
					copyRow(row2 - 1, row2);
				}

				clears++;
				grid[0] = deep_copy(emptyRow);
			}
		}

		game.awardClearScore(clears);
		currentPiece.addToGrid();
	},

	keyPressed: function(key) {
		if (config.ai) return;
		switch (key) {
			case 'w':
			case 'W':
				currentPiece.rotate(1);
				break;
			case 'a':
			case 'A':
				currentPiece.moveLeft();
				break;
			case 's':
			case 'S':
				currentPiece.moveDown();
				break;
			case 'd':
			case 'D':
				currentPiece.moveRight();
				break;
			case ' ':
				currentPiece.drop();
				break;
		}

		redrawField();
	},
};

/*
 *
 * @param obj{x,y}
 * @returns {Boolean}
 */
function isOnGrid(field) {
	var x = field.x;
	var y = field.y;

	return !(x < 0 || y < 0 || (x >= grid[0].length) || (y >= grid.length));
}

/**
 *
 * @param object {x,y}
 * @returns {Boolean}
 */
function isFieldEmpty(field) {
	if (isOnGrid(field)) {
		var color = getField(field);
		// equals colors[0] without needing to array-compare - performance
		return (color[0] === 10 && color[1] === 10 && color[2] === 10);
	} else {
		return true;
	}
}

/**
 *
 * DRAWING FUNCTIONS
 *
 **/

function setup() {
	createCanvas(field.width,field.height);
	redrawField();
	game.start();

	// web workers don't like windows
	if (window !== undefined) {
		window.onkeypress = function(event) {
		var key = '';

		if (window.event) { // IE
			key = event.keyCode;
		} else if (event.which) { // civilized world
			key = event.which;
		}

		if (!game.lost) {
			game.keyPressed(String.fromCharCode(key));
		}
	};

	}
}

function drawInfo() {
	var x = field.width*.9;
	textSize(20);
	textAlign(RIGHT);
	fill(255);
	text('score: ' + game.score, x, 80);
	text('lines cleared: ' + game.totalRowsCleared, x, 120);
	text('pieces spawned: '+game.piecesDealt, x, 160);
}

function draw() {
	game.tick();
}

function copyRow(src,dest) {
	if (src < 0) {
		grid[dest] = deep_copy(emptyRow);
	} else {
		grid[dest] = deep_copy(grid[src]);
	}
}

function drawSquare(myField) {
	if (!config.draw) return;
	var pieceColorCode = getField(myField);

	var r = pieceColorCode[0];
	var g = pieceColorCode[1];
	var b = pieceColorCode[2];

	var x = myField.x * field.pixelSize;
	var y = myField.y * field.pixelSize;

	stroke(0);
	fill(r,g,b);
	rect(x,y,field.pixelSize,field.pixelSize);
}

function drawGrid() {
	var field = {
		x:0,
		y:0,
	};

	for (var x = 0; x < grid[0].length; x++) { // 10
		field.x = x;

		for (var y = 0; y < grid.length; y++) { // 20
			field.y = y;
			drawSquare(field);
		}
	}
}

function redrawField() {
	if (config.draw) {
		background(field.bgcolor);
		drawGrid();
		stroke(153);
		line(field.width/2,0,field.width/2,field.height);
		drawInfo();
	}
}

/**
 *
 * HELPER FUNCTIONS
 * mostly grid interaction
 *
 */

function time() {
	return Date.now();
}

function deep_copy(array) {
	try {
		return JSON.parse(JSON.stringify(array));
	} catch (Error) {
		console.trace('copy kabumm');
	}
}

function getField(field) {
	return grid[field.y][field.x];
}

function setField(field,color) {
	grid[field.y][field.x] = colors[color];
}

function isRowFull(row) {
	for (var i = 0; i < grid[row].length; i++) {
		var field = {
			x: i,
			y: row,
		};

		if (isFieldEmpty(field)) return false;
	}

	return true;
}

function isRowEmpty(row) {
	for (var i = 0; i < grid[row].length - 1; i++) {
		var field = {
			x: i,
			y: row,
		};

		if (!isFieldEmpty(field)) return false;
	}

	return true;
}


/**
 *
 * rotates a given matrix 90° to the right around its center
 * @param array matrix
 * @return array
 */

function rotateMatrix(matrix) {
	var newMatrix = deep_copy(matrix);

	for (var row = 0; row < matrix.length; row++) {
		var newRow = [];
		for (var col = 0; col < matrix[0].length; col++) {
			newRow.push(matrix[matrix[0].length - col - 1][row]);
		}

		newMatrix[row] = newRow;
	}

	return newMatrix;
}

var RNG = {
	border: 2147483647,
	seed: 1,

	generate: function() {
		var rand = this.seed * 16807 % this.border;
		this.seed = rand;

		return (rand - 1) / this.border;
	},

	reset: function() {
		this.seed = 1;
	},
};
