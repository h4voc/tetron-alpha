You are viewing my personal tetris AI! 
The original goal was to create an AI that plays tetris on a roughly 6" screen powered by my Raspberry Pi 3.
The project is already a success, so further development might stagnate. A bit. A few years between commits maybe, nothing serious.

Here's the run-down:
pop up index.html and you can play a semi-complete game of tetris.
As in, there are no levels, the tiles don't drop automatically, because the main reason for tetris being there is the AI part.

Which brings us to the browser console (F12 in Firefox). The control GUI being let's call it limited, is intentional.

By default, the AI is set to demo mode. You can start it up by typing "tetron.start()"
or hitting the only button on the screen.

Alternatively, you can switch to training mode, with the following line: "tetron.config.demoMode = false"

Thing is, you won't see anything in this mode, because the training is done on sub-threads (webworkers),
and you can't even play tetris anymore, because as of now I could not be bothered to fix that. (type "config.ai = false" to fix it yourself)

other useful functions:
"game.tickRate = 100" // or whatever you'd like, milliseconds per move.
-- This does not impact the quality of the move,
-- and caps at the time the AI needs to calculate the move

"tetron.config.pause = (true/false)" should be trivial
-- Note: this pauses everything. 
-- threads already working (in training mode) will finish their game, then wait.

and that's it. Feel free to play around with any settings you get your hands on


