/* global game, currentPiece, grid, config */

var tetron = {
	config: {
		populationSize: 300,
		mutationRate: 0.03,
		generations: 1, // per run. can (and should) run multiple times
		survivalRate: (1/4),
		demoMode: true,
		displayMode: true,
		pause: false,
		tps: {
			show: false,
			interval: 5,
		},

		workers: {
			count: 5,
			verbose: false,
			tickRate: 100,
		},
	},

	tps: 0,
	tpsReset: 0,

	bestSpecimen: {
		weights: {},
		score: 0,
	},

	start: function() {
		config.ai = true;
		//game.reset();
		if (tetron.config.demoMode) {
			if (JSON.stringify(tetron.specimen.weights) === "{}") {
				//console.log('doing demo with random weights. Are you sure you pushed the right button?');
				tetron.specimen.applyWeights(tetron.specimen.defaultWeights);
			}

			if (!game.lost) {
				// run the AI
				tetron.run();
			}
		} else {
			tetron.population.data.generation = 0;
			tetron.population.data.specimenIndex = 0;
			tetron.trainAI();
		}
	},

	// if anyone is reading this, thinking 'moar performance!',
	// everything in here is a good place to start.
	step: function() {
		// the tps block
		if (tetron.config.tps.show) {
			if (time() > tetron.tpsReset+(1000*tetron.config.tps.interval)) {
				tetron.tpsReset = time();
				console.log('TPS: '+(tetron.tps/tetron.config.tps.interval));
				tetron.tps = 0;
			} else {
				tetron.tps++;
			}
		}

		// todo: encapsulate this block for the worker object
		if (!game.lost) {
			var possibleMoves = tetron.getAllPossibleMoves();
			var bestMove = tetron.specimen.calculateBestMove(possibleMoves);
			tetron.executeMove(bestMove);
			redrawField();
		} else return game.totalRowsCleared;
	},

	// for demo mode
	run: function() {
		if (!game.lost) {
			if (!tetron.config.pause) tetron.step();
			setTimeout(tetron.run, game.tickRate);
		} else {
			return game.score;
		}
	},

	populate: function() {
		if (tetron.population.current.length === 0) {
			tetron.population.current = tetron.population.createRandom();
			tetron.population.evaluated = [];
			tetron.population.data.generation = 0;
			tetron.population.data.specimenIndex = 0;
			console.log('populated');
		}
	},

	trainAI: function() {
		game.tickRate = 100; // this is the main thread, let's not block it
		if (tetron.config.pause) {
			setTimeout(tetron.trainAI, game.tickRate);
			return;
		}

		// foreach generation
		if (tetron.population.data.generation < tetron.config.generations) {
			tetron.populate(); // if neccessary, that is

			var todo = tetron.population.getNonEvaluated(tetron.population.current);
			// generation done? (actual work done by web workers)
			if (todo.length !== 0) {
				console.log('todo length: '+todo.length);
				if (!tetron.population.workStarted) {
					var queue = [];
					for (var index in todo) {
						var specimenId = todo[index];
						if (tetron.population.checkForClone(specimenId)) {
							console.log('identified specimen '+todo[index]+' as a clone');
							tetron.population.clones++;
						} else queue.push(specimenId);
					}

					console.log('pushing queue size: '+queue.length);
					workers.commitQueue(queue); // working copy
					tetron.population.workStarted = true;
				} else {
					// just waiting here, no need to waste cpu cycles
					setTimeout(tetron.trainAI, game.tickRate * 100);
					return;
				}
			} else { // evolve
				tetron.population.evolve(tetron.population.current);
				console.log('number of clones last generation: '+tetron.population.clones);
				tetron.population.clones = 0;
				tetron.population.workStarted = false;
			}

			setTimeout(tetron.trainAI, game.tickRate);
		} else { // done
			console.log('done training AI at generation '+tetron.config.generations);
			tetron.population.exportCurrentPopulation();
		}

	},

	executeMove: function(state) {
		game.importGameState(state);
		currentPiece.addToGrid();
	},

	getHolesInColumn: function(x) {
		var field = {
			x: x,
			y: 0,
		};

		var result = 0;
		var hitBrick = false;

		for (var y in grid) {
			field.y = y;

			if (hitBrick && isFieldEmpty(field)) result++;
			else if (!hitBrick && !isFieldEmpty(field)) hitBrick = true;
		}

		return result;
	},

	getRoofiesInColumn: function(x) {
		var field = {
			x: x,
			y: 0,
		};

		var result = 0;
		var hitHole = false;

		for (var y = grid.length - 1; y >= 0; y--) {
			field.y = y;
			if (!hitHole && isFieldEmpty(field)) hitHole = true;
			else if (hitHole && !isFieldEmpty(field)) result++;
		}

		return result;
	},

	getColumnHeights: function() {
		var result = [];

		var field = {
			x: 0,
			y: 0,
		};

		for (var x in grid[0]) {
			field.x = x;
			var foundBrick = false;
			for (var y in grid) {
				field.y = y;

				if (!isFieldEmpty(field)) {
					result.push(grid.length - y);
					foundBrick = true;
					break;
				}
			}

			if (!foundBrick) result.push(0);
		}

		return result;
	},

	getMaxHeight: function() {
		var heights = tetron.getColumnHeights();
		var max = 0;

		for (var index in heights) {
			if (heights[index] > max) max = heights[index];
		}

		return max;
	},

	getAllPossibleMoves: function() {
		var possibleMoves = [];

		for (var rotation = 0; rotation < 4; rotation++) {
			currentPiece.rotate(rotation);
			while (currentPiece.moveLeft()) {} // just move left all the way

			do { // this has to be the first time I effectively used a do-while
				var preMoveState = game.exportCurrentGameState();
				currentPiece.drop();
				// dropping will spawn the next piece, let's remove that for the sake of evaluation
				// (especially because the AI is not supposed to know about that)
				currentPiece.removeFromGrid();
				possibleMoves.push(game.exportCurrentGameState());
				game.importGameState(preMoveState);
			} while (currentPiece.moveRight());
		}

		return possibleMoves;
	},

	createRandomWeightSet: function() {
		var weights = {
			maxHeight: Math.random() - 0.5,
			//maxHeightSquared: Math.random() - 0.5,
			//maxHeightSquaredConfigPower: Math.random() * 2,
			totalHeight: Math.random() - 0.5,
			holes: Math.random() - 0.5,
			roughness: Math.random() - 0.5,
			scoreDifference: Math.random() - 0.5,
			roofies: Math.random() - 0.5,
		};

		return weights;
	},

	population: {
		workStarted: false,
		clones: 0,
		// as before: data container
		data: {
			specimenIndex: 0,
			generation: 0,
		},

		current: [], // a population is an array of specimen objects (== weight objects)
		evaluated: [], // an array of evaluatedSpecimen objects - containing a score and a specimen

		getNonEvaluated: function(population) {
			var result = [];

			for (var index in population) {
				if (population[index].score === undefined) result.push(index);
			}

			return result;
		},

		// aaaand logic
		importPopulation: function(population) {
			tetron.population.current = JSON.parse(population);
		},

		exportCurrentPopulation: function() {
			return JSON.stringify(tetron.population.current);
		},

		checkForClone: function(index) {
			var specimen = tetron.population.current[index];

			var weightKeys = Object.keys(specimen.weights);

			for (var spec in tetron.population.current) {
				var otherSpecimen = tetron.population.current[spec];
				if (otherSpecimen === specimen) continue; // it's equal to itself ofc
				if (otherSpecimen.score === undefined) continue; // don't kill the first one

				// check equality weight by weight
				var hasEqualWeights = true;
				for (var key in weightKeys) {
					var value = weightKeys[key];

					if (otherSpecimen.weights[value] !== specimen.weights[value]) {
						hasEqualWeights = false;
						break;
					}
				}

				// clone of 1 --> let's get out of here
				if (hasEqualWeights) {
					specimen.score = otherSpecimen.score;
					return true;
				}
			}

			// no clone found
			return false;
		},

		collectSurvivors: function(evaluatedPop) {
			var oldPop = deep_copy(evaluatedPop);
			var highscore = tetron.population.getHighscore(oldPop);

			console.log('highscore for generation '+tetron.population.data.generation+': '+highscore);
			console.log('average score for generation: '+tetron.population.getAvgScore(oldPop));

			var nextGen = [];

			do {
				highscore = tetron.population.getHighscore(oldPop);

				if (highscore === 0) {
					if (nextGen.length === 0) {
						tetron.population.current = [];
						console.log('throwing out the trash and starting fresh');
						tetron.populate();
						return tetron.population.current;
					} else break;
				}

				var survivors = 0;
				for (var index in oldPop) {
					var currentSpecimen = oldPop[index];
					if (currentSpecimen === undefined) continue; // already survived, don't clone anyone

					var survivalChance = (currentSpecimen.score / highscore);

					if (Math.random() < survivalChance) {
						oldPop[index] = undefined; // kill the pointer to prevent cloning
						nextGen.push(currentSpecimen);
						survivors++;
					}
				}

				console.log('survivors: '+survivors);
			} while (nextGen.length < tetron.config.populationSize * tetron.config.survivalRate);

			console.log('survivors from generation: '+(nextGen.length));
			return nextGen;
		},

		getRandomSubPopulation: function(pop, percentile) {
			var result = [];

			while (result.length < tetron.config.populationSize * (percentile / 100)) {
				var randomIndex = Math.floor(Math.random() * pop.length);
				result.push(pop[randomIndex]);
			}

			return result;
		},

		breed: function(population, count) {
			var children = [];
			while (children.length < count) {
				var popA = tetron.population.getRandomSubPopulation(population, 10);
				var popB = tetron.population.getRandomSubPopulation(population, 10);

				var specimenA = tetron.population.getBestIndividual(popA);
				var specimenB = tetron.population.getBestIndividual(popB);

				// we want to prevent clones, this isn't star wars.
				// and it went wrong there, too!
				if (specimenA !== specimenB) {
					var newSpecimen = genetics.breed(specimenA, specimenB);
					children.push(newSpecimen);
				}
			}

			return children;
		},

		evolve: function(population) {
			var evaluatedPopulation = population;
			console.log('evolving');
			console.log(deep_copy(evaluatedPopulation));

			var survivors = tetron.population.collectSurvivors(evaluatedPopulation);

			// breed
			var descendantCount = tetron.config.populationSize - survivors.length;
			var descendants = tetron.population.breed(evaluatedPopulation, descendantCount);

			var mutations = 0;
			// and mutate
			for (var index in descendants) {
				var specimen = descendants[index];
				mutations += genetics.mutate(specimen);
			}

			var nextGen = survivors.concat(descendants);

			console.log('mutations: '+mutations);
			tetron.population.current = nextGen;
			tetron.population.evaluated = [];
			tetron.population.data.specimenIndex = 0;
			tetron.population.data.generation++;
		},

		createRandom: function() {
			console.log('creating population');
			var population = [];

			for (var i = 0; i < tetron.config.populationSize; i++) {
				var weights = tetron.createRandomWeightSet();

				var specimen = {
					weights: weights,
					score: undefined,
				};
				population.push(specimen);
			}

			console.log('done populating');
			return population;
		},

		getBestIndividual: function(population) {
			var best = undefined;

			for (var index in population) {
				var specimen = population[index];
				if (best === undefined || specimen.score > best.score) best = specimen;
			}

			return best;
		},

		getHighscore: function(population) {
			var score = 0;

			for (var index in population) {
				var specimen = population[index];
				if (specimen === undefined) continue;
				var specimenScore = specimen.score;
				if (specimenScore > score) score = specimenScore;

				if (specimenScore > tetron.bestSpecimen.score) tetron.bestSpecimen = specimen;
			}

			return score;
		},

		getTotalScore: function(evaluatedPopulation) {
			var score = 0;

			for (var index in evaluatedPopulation) {
				var specimen = evaluatedPopulation[index];
				score += specimen.score;
			}

			return score;
		},

		getAvgScore: function(evaluatedPopulation) {
			var score = tetron.population.getTotalScore(evaluatedPopulation);

			score = score / evaluatedPopulation.length;

			return Math.floor(score);
		},
	},

	specimen: {
		// data container
		// score for this: 87640
		defaultWeights: { // hard coded values for demo mode - yes, these are evolved
			maxHeight: -0.0150540132725246, // highest column
			totalHeight: -0.079, // all columns added together
			holes: -0.3446073608230351,	// empty fields with a field above
			roughness: -0.08508637234673544, // difference between highest and lowest column
			scoreDifference: 0.45984507475579084, // rewarding clears
			roofies: -0.031166708778108343, // blocks above holes
		},

		weights: {},

		// and logic

		importSpecimen: function(specimen) {
			tetron.specimen.applyWeights(JSON.parse(specimen.weights));
		},

		exportSpecimen: function() {
			return JSON.stringify(tetron.specimen.weights);
		},

		exportBestSpecimen: function() {
			return JSON.stringify(tetron.bestSpecimen);
		},

		evaluate: {
			maxHeight: function() {
				return (tetron.getMaxHeight() * tetron.specimen.weights.maxHeight);
			},

			maxHeightSquared: function() {
				var power = tetron.specimen.weights.maxHeightSquaredConfigPower;
				var height2 = Math.pow(tetron.getMaxHeight(), power);
				return height2 * tetron.specimen.weights.maxHeightSquared;
			},

			totalHeight: function() {
				var result = 0;
				var heights = tetron.getColumnHeights();

				for (var index in heights) {
					result += heights[index];
				}

				result = result * tetron.specimen.weights.totalHeight;
				return result;
			},

			holes: function() {
				var result = 0;

				for (var x in grid[0]) {
					result += tetron.getHolesInColumn(x);
				}

				result = result * tetron.specimen.weights.holes;
				return result;
			},

			scoreDifference: function(state, oldState) {
				var prevScore = oldState.score;
				var newScore = state.score;
				return (newScore - prevScore) * tetron.specimen.weights.scoreDifference;
			},

			roughness: function() {
				var heights = tetron.getColumnHeights();
				var roughness = 0;

				for (var index = 0; index < heights.length - 1; index++) {
					var a = heights[index];
					var b = heights[index+1];

					if (a < b) roughness += (b - a);
					else roughness += (a - b);
				}

				return (roughness * tetron.specimen.weights.roughness);
			},

			// blocks above holes
			roofies: function() {
				var result = 0;
				for (var x in grid[0]) {
					result += tetron.getRoofiesInColumn(x);
				}

				result = result * tetron.specimen.weights.roofies;

				return result;
			},
		},

		applyWeights: function(weights) {
			this.weights = weights;
		},

		calculateBestMove: function(moves) {
			var bestMove = {
				state: {},
				value: undefined,
			};

			var oldState = game.exportCurrentGameState();
			for (var index in moves) {
				var currentMove = moves[index];
				game.importGameState(currentMove);
				var value = 0;

				value += this.evaluate.maxHeight();
				//value += this.evaluate.maxHeightSquared();
				value += this.evaluate.totalHeight();
				value += this.evaluate.holes();
				value += this.evaluate.scoreDifference(currentMove, oldState);
				value += this.evaluate.roughness();
				value += this.evaluate.roofies();

				if (bestMove.value === undefined || value > bestMove.value) {
					bestMove.state = currentMove;
					bestMove.value = value;
				}

				game.importGameState(oldState);
			}

			return bestMove.state;
		},

		getRandomProperty: function(object) {
			var keys = Object.keys(object);
			var index = Math.floor(Math.random() * keys.length);

			return keys[index];
		},
	},
};

var genetics = {
	breed: function(specimenA, specimenB) {
		var child = {
			weights: {},
			score: undefined,
		};

		var keys = Object.keys(specimenA.weights);

		for (var index in keys) {
			var prop = keys[index];
			child.weights[prop] = binaryRandom() ? specimenA.weights[prop] : specimenB.weights[prop];
		}

		return child;
	},

	mutate: function(specimen) {
		var mutations = 0;
		// at 0.1: 10% for 1 value, 1% for 2 values and so on
		while (Math.random() < tetron.config.mutationRate) {
			mutations++;
			specimen.score = undefined; // mutations change gameplay --> reset scoring
			var property = tetron.specimen.getRandomProperty(specimen.weights);
			specimen.weights[property] += (Math.random() - 0.5);
		}

		return mutations;
	},
};

function binaryRandom() {
	var number = Math.random();

	return (number > 0.5);
}

var workers = {
	workers: [],
	queue: [],

	commitQueue: function(queue) {
		this.queue = queue;
		tetron.population.workStarted = true;
		console.log('starting work queue');
		this.work();
	},

	isAnyOneWorking: function() {
		for (var index in this.workers) {
			if (!this.workers[index].free) return true;
		}

		return false;
	},
	// if this returns true, work is done
	work: function() {
		if (tetron.config.pause) {
			setTimeout(workers.work, game.tickRate * 2);
			return;
		}

		if (workers.queue.length > 0) {
			tetron.config.workers.verbose && console.log('found work and looking for available worker');
			var worker = workers.getAvailableWorker();
			if (worker === false) {
				setTimeout(workers.work, game.tickRate * 10);
				tetron.config.workers.verbose && console.log('no workers available, trying again in 100 game ticks');
				return false;
			} else {
				// take first card out of queue,
				// give to chosen worker,
				// and yell at him in german because work
				// - repeat until done
				var specimenIndex = workers.queue.splice(0,1)[0];
				var specimen = tetron.population.current[specimenIndex];

				setTimeout(workers.work, game.tickRate);
			//	if (worker.id === 3) console.log(deep_copy(this.workers));

				return workers.startTask(worker, specimen, specimenIndex);
			}

		} else return !workers.isAnyOneWorking();
	},

	getAvailableWorker: function() {
		// look for a free worker
		for (var index in this.workers) {
			var worker = this.workers[index];
			if (worker.free) {
				tetron.config.workers.verbose && console.log('worker '+index+' seems to be free');
				return worker;
			}
		}

		if (this.workers.length < tetron.config.workers.count) {
			return this.create(this.workers.length);
			// note: worker deletion before the end of the script is not recommended
		} else return false;
	},

	startTask: function(worker, specimen, genomeIndex) {
		if (!worker.free) return false;
		worker.free = false;
		worker.specimenId = genomeIndex;

		var message = {
			workerId: worker.id,
			specimenId: genomeIndex,
			weights: specimen.weights,
		};

		worker.object.postMessage(message);
		tetron.config.workers.verbose && console.log('pushing specimen '+genomeIndex+' to worker '+worker.id);
		return true;
	},

	create: function(id) {
		console.log('creating worker object');
	//	console.trace();
		var worker = {
			id: id,
			specimenId: undefined,
			object: new Worker('./workers/specimenWorker.js'),
			free: true,
		};

		worker.object.onmessage = function(message) {
			workers.handleResponse(message);
		};

		this.workers.push(worker);
		return worker;
	},

	handleResponse: function(message) {
		var data = message.data;
		var worker = workers.workers[message.data.workerId];
		worker.free = true;
		worker.specimenId = undefined;

		var specimen = tetron.population.current[data.specimenId];
		if (specimen.score !== undefined) console.log('error! specimen has score '+specimen.score);
		specimen.score = data.score;

		tetron.config.workers.verbose && console.log('worker '+worker.id+' done with specimen '+data.specimenId);
	},

	test: function() {
		var worker = this.getAvailableWorker();
		workers.startTask(worker, 'testGenome', 5);
	},
};